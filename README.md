# auvua_dz2
Simulation package for Danger Zona 2.
This simulation uses Gazebo with the `freefloating_gazebo` plugin.

### Build
First, create a catkin workspace:
```
$ mkdir -p ~/catkin_ws/src
$ cd ~/catkin_ws/src
$ catkin_init_workspace
```

Then, clone this repo into `src`:
```
$ git clone https://gitlab.com/forbesk9/auvua_dz2
```

Next, install the dependencies:
```
$ cd auvua_dz2
$ rosdep install --from-paths . --ignore-src --rosdistro=jade -y
```

Finally, build the package and setup the environment variables (you'll need to source the setup file in any new terminal windows, or add it to your `.bashrc` file):
```
$ cd ~/catkin_ws
$ catkin_make
$ source devel/setup.bash
```

### Usage
Use `roslaunch` to run the simulation:
```
$ roslaunch auvua_dz2 basic_dz2_world.launch
```
